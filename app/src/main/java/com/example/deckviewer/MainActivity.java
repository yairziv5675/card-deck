package com.example.deckviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private Card current; /* The current card object */
    private ListView lvDeck; /* The listView which contains the cards on the screen */
    private int currentIndex; /* The index of the current card in the pack */
    private ImageView ivCurrent; /* The image of the current card that is displayed on the screen */
    private Button btnReshuffle; /* The button which reshuffles the pack of cards*/
    private EditText etCardNumInput; /* The user's input field */
    private ArrayList<Card> deck; /* The pack of cards which contains every card */
    private TextView tvCurrentName; /* The description of the current card on the screen */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Filling up our pack of cards:
        this.deck = new ArrayList<>();

        for (String suit : Card.SUIT_NAMES) {
            for (int value = 1; value < 14; value++) {
                this.deck.add(new Card(suit, value, true));
            }
        }

        // Setting the listView and connecting it to our deck:
        lvDeck = findViewById(R.id.lv_deck);
        initAdapter();
        lvDeck.setOnItemClickListener(this);
        lvDeck.setOnItemLongClickListener(this);

        // Setting the first current card to the first card in the pack:
        this.currentIndex = 0;
        this.current = this.deck.get(this.currentIndex);

        // Setting and updating the image of the currently displayed card:
        this.ivCurrent = findViewById(R.id.chosen_card);
        updateImageViewByString(this.current.getCardImageName(), this.ivCurrent);

        // Setting and updating the description on the screen for the card:
        this.tvCurrentName = findViewById(R.id.card_name);
        updateCardName();

        // Setting the reshuffle button:
        this.btnReshuffle = findViewById(R.id.reshuffle_btn);
        this.btnReshuffle.setOnClickListener(this);

        // Setting the user's input field:
        this.etCardNumInput = findViewById(R.id.card_num_input);
        this.etCardNumInput.addTextChangedListener(this);
    }

    /**
     * Initializes the adapter and connects it to the listView, makes it easier to change the style
     * of the adapter later on if needed.
     */
    private void initAdapter() {
        /* The adapter for the listView to present the deck */
        ArrayAdapter<Card> adapter = new ArrayAdapter<>(this,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, this.deck);
        this.lvDeck.setAdapter(adapter);
    }

    private void updateImageViewByString(String drawableName, ImageView imgv) {
        int id = getResources().getIdentifier(drawableName, "drawable",
                this.getPackageName());
        imgv.setImageResource(id);
    }

    public void moveInPack(View view) {
        // If the button clicked was the forward button:
        if (view.getId() == R.id.fwd_btn) {
            updateCurrent(this.currentIndex + 1);
        }
        // If the button clicked was the backwards button:
        else {
            updateCurrent(this.currentIndex - 1);
        }
    }

    private void updateCurrent(final int newIndex) {
        // Checking if the new index is larger than/equal to 0:
        boolean valid = newIndex >= 0;
        // Checking if the new index is smaller than the length of the deck:
        valid &= newIndex < this.deck.size();

        // If the new index is valid:
        if (valid) {
            // Updating the current index:
            this.currentIndex = newIndex;

            // Updating the card:
            this.current = this.deck.get(this.currentIndex);

            // Updating the photo of the card:
            updateImageViewByString(this.current.getCardImageName(), this.ivCurrent);

            // Updating the description of the displayed card:
            updateCardName();
        }
    }

    /**
     * Updates the name of the card displayed on the screen.
     */
    private void updateCardName() {
        Resources res = getResources();
        // The character-symbols for every suit in the pack:
        final String[] SUITS_SYMBOLS = res.getStringArray(R.array.suit_symbols);

        // Finding the correct symbol for the current card:
        int suitIdx = 0;
        for (int i = 0; i < Card.SUIT_NAMES.length; i++) {
            if (Card.SUIT_NAMES[i].equals(this.current.getSuit())) {
                suitIdx = i;
                break;
            }
        }

        // Getting the name of the current card + its suit symbol:
        final String currentName = this.current.getCardName() + " " + SUITS_SYMBOLS[suitIdx];
        // Updating the text on the screen:
        this.tvCurrentName.setText(String.format(res.getString(R.string.chosen_card_txt),
                currentName));
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // Preventing the user from typing more than 2 digits:
        final int CHAR_LIM = 2;
        if (charSequence.length() > CHAR_LIM) {
            // Resizing the digits back to 2 digits and preserving the selection index of the input:
            this.etCardNumInput.setText(charSequence.subSequence(0, CHAR_LIM));
            this.etCardNumInput.setSelection(CHAR_LIM);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        final String input = editable.toString();
        // Making sure the text isn't empty:
        if (!input.isEmpty()) {
            // Changing the current index to the new one:
            final int newIdx = Integer.parseInt(input) - 1;
            updateCurrent(newIdx);
        }
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        // If the user pressed the 'Reshuffle' button:
        if (id == this.btnReshuffle.getId()) {
            // Reshuffling the deck of cards:
            Collections.shuffle(this.deck);

            // Setting the listView's adapter to show the new list:
            initAdapter();

            // Deleting the input of the user:
            this.etCardNumInput.setText("");

            // Setting the current card to the first card in the reshuffled pack:
            updateCurrent(0);

            // Making a little toast message appear:
            Toast.makeText(this, "Successfully Reshuffled!", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("DefaultLocale")
    private void onItemClick(int pos) {
        // Updating the card:
        updateCurrent(pos);
        // Updating the editText on the screen:
        this.etCardNumInput.setText(String.format("%d", pos + 1));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        this.onItemClick(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        Toast.makeText(this, this.deck.get(position).toString(), Toast.LENGTH_SHORT).show();
        return false;
    }
}
