package com.example.deckviewer;

import androidx.annotation.NonNull;


import java.util.Arrays;
import java.util.Random;

public class Card {
    private final String suit; /* The shape of the car, i.e: diamond / heart / spade / club */
    private final int value; /* The value of the card, ranging from 2 to 14 (14 is ace) */
    private boolean isFaceUp; /* Whether the card is hidden or revealed */

    private static final Random rnd = new Random(); /* A variable used to generate random values */

    // A constant array of the different shapes' names, allows us to automatically create cards:
    public static final String[] SUIT_NAMES = {"Diamond", "Heart", "Spade", "Club"};


    // A constant array of the different values' names. First 2 indices are empty so that the value
    // of the card corresponds to the index of its name in the array:
    private static final String[] CARD_NAMES = {"", "", "2", "3", "4", "5", "6", "7", "8", "9",
                                                "10", "Jack", "Queen", "King", "Ace"};

    // A shortened version of CARD_NAMES that allows us to find the image of the card automatically
    // by adding the first letter of the card's suit and its code in this array:
    private static final String[] CARD_CODES = {"", "", "2", "3", "4", "5", "6", "7", "8", "9",
                                                "10", "j", "q", "k", "a"};

    // The card's back image name:
    private static final String BACK_IMG = "back";

    public Card() {

        // Randomly choosing a suit:
        this.suit = SUIT_NAMES[rnd.nextInt(4)];

        // Randomly setting a values:
        this.value = rnd.nextInt(13) + 2;

        // Setting the default state of the card to hidden:
        this.isFaceUp = false;
    }

    public Card(String suit, int value, boolean isFaceUp) {
        // Setting the given suit:
        this.suit = suit;
        // Making sure the given suit is a valid one:
        int suitIndex = Arrays.binarySearch(SUIT_NAMES, suit);
        if (suitIndex < 0) {
            System.err.printf("Invalid suit: %s", suit);
        }

        // Setting the values to a range between 2-14:
        value = Math.max(1, Math.min(14, value));
        // If the value is 1, set it to 14 as it's the ace:
        if (value == 1) {
            value = 14;
        }
        this.value = value;

        // Setting the given state of the card (hidden/revealed):
        this.isFaceUp = isFaceUp;
    }

    public String getSuit() {
        return suit;
    }

    public int getValue() {
        return value;
    }

    public boolean isFaceUp() {
        return isFaceUp;
    }

    public void setFaceUp(boolean faceUp) {
        isFaceUp = faceUp;
    }

    /**
     * Returns a string of "Red" or "Black" depending on the color of the card.
     * @return "Red" if the card's suit is red, "Black" otherwise.
     */
    private String calculateColor() {
        // Using the order of SUIT_NAMES to our advantage and deciding the color based on the index:
        int suitIndex = Arrays.binarySearch(SUIT_NAMES, this.suit);
        if (suitIndex < 2) {
            return "Red";
        }
        else {
            return "Black";
        }
    }

    public String getColor() {
        return calculateColor();
    }

    @NonNull
    @Override
    public String toString() {
        return "Card{" +
                "suit='" + suit + '\'' +
                ", value=" + CARD_NAMES[value] +
                ", isFaceUp=" + isFaceUp +
                '}';
    }

    /**
     * Flips the card from hidden to revealed or vice versa depending on its former state.
     */
    public void turn() {
        this.isFaceUp = ! this.isFaceUp;
    }

    /**
     * Calculates the card's current image which is to be presented in the app based on the card's
     * attributes.
     * @return The name of the image that fits the card's current state.
     */
    public String getCardImageName() {
        String result;
        if (this.isFaceUp) {
            // The image is the first letter of the suit + the code for the card:
            result = this.suit.toLowerCase().charAt(0) + CARD_CODES[value];
        }
        else {
            // If the card is hidden then the image is always the back image:
            result = BACK_IMG;
        }
        return result;
    }

    /**
     * Returns the card's true name, i.e: "Diamond Queen" or "Spade 6".
     * @return The card's true name, i.e: "Diamond Queen" or "Spade 6".
     */
    public String getCardName() {
        return this.getSuit() + " " + CARD_NAMES[this.value];
    }
}
